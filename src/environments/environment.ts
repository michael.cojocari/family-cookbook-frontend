export const environment = {
  production: false,
  // familyCookbookBackendUrl: 'http://localhost:3000/',
  familyCookbookBackendUrl: 'http://192-168-0-101.mcojocari.direct.quickconnect.to:3000/',
  recipesStub: 'recipes',
  authorsStub: 'authors',
};
