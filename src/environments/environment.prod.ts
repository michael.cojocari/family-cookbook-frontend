export const environment = {
  production: true,
  familyCookbookBackendUrl:
    'http://192-168-0-101.mcojocari.direct.quickconnect.to:3000/',
  recipesStub: 'recipes',
  authorsStub: 'authors',
};
