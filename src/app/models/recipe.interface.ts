import { Ingredient } from './ingredient.interface';
import { AuthorUser } from './author-user.interface';

export interface Recipe {
  Id: string;
  appliance: string;
  ingredients: Ingredient[];
  name: string;
  timeToMake: string;
  instructions: string;
  portions: number;
  authorUser: AuthorUser;
}
