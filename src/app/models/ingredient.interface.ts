export interface Ingredient {
  amount: string;
  type: string;
  name: string;
}
