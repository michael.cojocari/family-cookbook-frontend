export interface RecipeResponse {
  status: number
  message: string
  data: Record<string, any>
}


