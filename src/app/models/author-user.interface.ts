export interface AuthorUser {
  firstName: string;
  lastName: string;
  email: string;
}
