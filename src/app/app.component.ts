import { Component, OnInit, isDevMode } from '@angular/core';
import { SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  public socialUser!: SocialUser;
  constructor(private socialAuthService: SocialAuthService) {
    this.socialUser = {
      idToken:
        'eyJhbGciOiJSUzI1NiIsImtpZCI6IjVkZjFmOTQ1ZmY5MDZhZWFlZmE5M2MyNzY5OGRiNDA2ZDYwNmIwZTgiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJuYmYiOjE2Nzg1NjUwOTUsImF1ZCI6IjI1OTg1NDIzMzYzLTk0MWgzYWtuamR1YWpvNzJhbHF0YjVza2ozZmY3cDUzLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTA1OTY4MTA3OTg3MTc1MTk4NzIxIiwiZW1haWwiOiJtaWNoYWVsLmNvam9jYXJpQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhenAiOiIyNTk4NTQyMzM2My05NDFoM2FrbmpkdWFqbzcyYWxxdGI1c2tqM2ZmN3A1My5hcHBzLmdvb2dsZXVzZXJjb250ZW50LmNvbSIsIm5hbWUiOiJNaWNoYWVsIENvam9jYXJpIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hL0FHTm15eFl1WVZEQ3phSEdIaTdlNFZBb2RJMG5SbFJ0azRKSGEzR3UxYzhUd0E9czk2LWMiLCJnaXZlbl9uYW1lIjoiTWljaGFlbCIsImZhbWlseV9uYW1lIjoiQ29qb2NhcmkiLCJpYXQiOjE2Nzg1NjUzOTUsImV4cCI6MTY3ODU2ODk5NSwianRpIjoiYzQ4ZjAzOTc4NzRhMmFjMjJlMjM5MDA1NDRkZWZiN2Q2MGJmOWU2OSJ9.Xw6KymJrtUtQRR2ymX13OkdeP6nsf-ZsgQvmMUErN1oyq8-8peCNHCNUMLbfoWEZpQV2Uij_0B4eHXf7TFJ5hBP7-hiDn_bm907dy8I9MeN36pp2j0LsmJ7vxTNgV7Jn8KJ0BWlUpMAqSDZFeEFM8QlaETVA55Z_k4sgifE2QYHYrlA69sW6BCTpaAXXeGka-_uiQCe3X_npnGlXJThT74L0QBUZWjTBvNlSUtPaZ_LpJVQY9S9FVs4gU0AQC0nUKQ6aY4Veum7Rr28yvMwbS-7szjGg5fopTg1cV66NVy6duIyenMKjndfUkZBhNV1THseb9w_TSh43qkhq-f_2yw',
      id: '105968107987175198721',
      name: 'Michael Cojocari',
      email: 'michael.cojocari@gmail.com',
      photoUrl:
        'https://lh3.googleusercontent.com/a/AGNmyxYuYVDCzaHGHi7e4VAodI0nRlRtk4JHa3Gu1c8TwA=s96-c',
      firstName: 'Michael',
      lastName: 'Cojocari',
      provider: 'GOOGLE',
    } as SocialUser;
    // this.socialAuthService.authState.subscribe((user) => {
    //   this.socialUser = user;
    //   console.log(this.socialUser);
    // });
  }

  ngOnInit() {
    if (isDevMode()) {
      console.log('Development mode.');
    } else {
      console.log('Production mode.');
    }
  }

  logOut() {
    this.socialAuthService.signOut();
  }
}
