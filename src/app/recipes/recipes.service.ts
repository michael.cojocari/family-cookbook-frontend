import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from '../models/recipe.interface';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { RecipeResponse } from "../models/recipe-response.interface";

@Injectable({
  providedIn: 'root',
})
export class RecipesService {
  private recipeUrl = environment.familyCookbookBackendUrl + environment.recipesStub;
  private authorUrl = environment.familyCookbookBackendUrl + environment.authorsStub;
  constructor(private http: HttpClient) {}

  getAllRecipes(): Observable<RecipeResponse> {
    return this.http.get<RecipeResponse>(this.recipeUrl);
  }

  saveRecipe(recipe: Recipe, edit: boolean): Observable<Recipe> {
    if (edit) {
      return this.http
        .put<Recipe>(this.recipeUrl + '/' + recipe.Id, recipe)
        .pipe();
    } else {
      return this.http.post<Recipe>(this.recipeUrl, recipe).pipe();
    }
  }

  deleteRecipe(recipe: Recipe): Observable<Recipe> {
    return this.http
      .delete<Recipe>(this.recipeUrl + '/' + recipe.Id)
      .pipe();
  }

  getAuthors(): Observable<RecipeResponse> {
    return this.http.get<RecipeResponse>(this.authorUrl).pipe();
  }
}
