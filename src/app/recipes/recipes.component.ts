import { Component, OnInit } from '@angular/core';
import { RecipesService } from './recipes.service';
import { Recipe } from '../models/recipe.interface';
import { Ingredient } from '../models/ingredient.interface';
import { SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';
import { Table } from 'primeng/table';

@Component({
  selector: 'recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss'],
})
export class RecipesComponent implements OnInit {
  public recipes: Recipe[] = [];
  public authors: string[] = [];
  public displayAddEdit: boolean = false;
  public selectedRecipe: Recipe = {} as Recipe;
  public edit: boolean = false;
  public socialUser!: SocialUser;
  public selectedAuthor: string = '';

  constructor(private recipesService: RecipesService) {
    this.getAllRecipes();
    this.getAuthors();
  }

  ngOnInit(): void {}
  getAuthors(): void {
    this.recipesService.getAuthors().subscribe({
      next: (authorsResponse) => {
        console.log(authorsResponse);
        this.authors = authorsResponse.data['data'];
      },
    });
  }
  getAllRecipes(): void {
    this.recipesService.getAllRecipes().subscribe({
      next: (recipesResponse) => {
        this.recipes = recipesResponse.data['data'];

        // DEBUG CODE REMOVE
        // this.selectedRecipe = this.recipes[0];
        // this.edit = true;
        // this.displayAddEdit = true;
      },
    });
  }

  showAddEdit(recipeToEdit = null): void {
    if (recipeToEdit) {
      this.selectedRecipe = recipeToEdit;
      this.edit = true;
    } else {
      this.edit = false;
    }
    this.displayAddEdit = true;
  }

  convertIngredientsToString(ingredients: Ingredient[] = []) {
    if (ingredients.length == 0) {
      return '';
    }

    let ingredientsAsString: string = ingredients[0].name;
    for (let i: number = 1; i < ingredients.length; i++) {
      ingredientsAsString = ingredientsAsString + ', ' + ingredients[i].name;
    }
    return ingredientsAsString;
  }

  closeAddEdit($event: Recipe) {
    this.getAllRecipes();
    this.displayAddEdit = false;
    this.selectedRecipe = {} as Recipe;
  }

  deleteRecipe(recipe: Recipe) {
    this.recipesService.deleteRecipe(recipe).subscribe((recipe) => {
      console.log('Deleted recipe id ' + recipe.Id);
      this.getAllRecipes();
    });
  }

  clear(recipesTable: Table) {
    recipesTable.clear();
  }
}
