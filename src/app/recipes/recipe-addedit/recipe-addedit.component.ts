import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Recipe } from '../../models/recipe.interface';
import { RecipesService } from '../recipes.service';
import { Ingredient } from '../../models/ingredient.interface';
import { SocialAuthService, SocialUser } from '@abacritt/angularx-social-login';
import { lastValueFrom } from 'rxjs';
import { AuthorUser } from '../../models/author-user.interface';

@Component({
  selector: 'recipe-addedit',
  templateUrl: './recipe-addedit.component.html',
  styleUrls: ['./recipe-addedit.component.scss'],
})
export class RecipeAddeditComponent implements OnInit {
  @Input() display: boolean = false;
  @Input() recipe: Recipe = {} as Recipe;
  @Input() edit = false;
  @Output() itemSaved: EventEmitter<Recipe> = new EventEmitter<Recipe>();
  public displayAddIngredient: boolean = false;
  public newIngredient: Ingredient = {} as Ingredient;

  constructor(
    private recipeService: RecipesService,
    private socialAuthService: SocialAuthService
  ) {}

  ngOnInit() {}

  saveRecipe() {
    const authorUser = {} as AuthorUser;
    this.socialAuthService.authState.subscribe((user: SocialUser) => {
      authorUser.firstName = user.firstName;
      authorUser.lastName = user.lastName;
      authorUser.email = user.email;
    });

    this.recipe.authorUser = authorUser;

    this.recipeService
      .saveRecipe(this.recipe, this.edit)
      .subscribe((recipe) => {
        this.itemSaved.emit(recipe);
      });
  }

  cancelSave() {
    this.itemSaved.emit();
  }
  deleteIngredient(toDeleteIngredient: Ingredient) {
    const newIngredients: Ingredient[] = [];

    this.recipe.ingredients.forEach((ingredient) => {
      if (!this.ingredientMatches(toDeleteIngredient, ingredient)) {
        newIngredients.push(ingredient);
      }
    });

    this.recipe.ingredients = newIngredients;
  }

  ingredientMatches(
    ingredientOne: Ingredient,
    ingredientTwo: Ingredient
  ): boolean {
    return (
      ingredientOne.name === ingredientTwo.name &&
      ingredientOne.type === ingredientTwo.type &&
      ingredientOne.amount === ingredientTwo.amount
    );
  }

  showAddIngredient() {
    this.displayAddIngredient = true;
  }

  saveIngredient() {
    if (!this.recipe.ingredients) {
      this.recipe.ingredients = [];
    }

    this.recipe.ingredients.push(this.newIngredient);
    this.newIngredient = {} as Ingredient;

    this.displayAddIngredient = false;
  }
}
