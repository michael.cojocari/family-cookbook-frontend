import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeAddeditComponent } from './recipe-addedit.component';

describe('RecipeAddeditComponent', () => {
  let component: RecipeAddeditComponent;
  let fixture: ComponentFixture<RecipeAddeditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RecipeAddeditComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(RecipeAddeditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
